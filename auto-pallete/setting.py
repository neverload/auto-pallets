#!/usr/bin/python
# -*- coding: UTF-8 -*-
import constant

# strategy
LOOP_TIME = 1
INCREMENT = 700
CODE_INPUT_TIME = 4
SLEEP_INTERVAL = 0.01
TYPEWRITE_INTERVAL = 0.1

# settings
LONG_PAUSE = 0.3
NORMAL_PAUSE = 0.1
QUICK_PAUSE = 0.02
