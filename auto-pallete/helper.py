#!/usr/bin/python
# -*- coding: UTF-8 -*-
import pyautogui
import time

import constant

def selectAll(position):
    if position != None:
        pyautogui.click(position)
    if constant.OS == 'Darwin':
        pyautogui.hotkey('command', 'a')
    elif constant.OS == 'Windows':
        pyautogui.hotkey('ctrl', 'a')

def clearInput(position):
    if position != None:
        pyautogui.click(position)
    pyautogui.typewrite(['delete'])

def setPause(pause):
    pyautogui.PAUSE = pause

def getTime():
    return int(time.time())

def switchWindow():
    if constant.OS == 'Darwin':
        pyautogui.hotkey('command', 'tab')
    elif constant.OS == 'Windows':
        pyautogui.hotkey('alt', 'tab')

def switchLeft():
    # print('switching left')
    pyautogui.click(constant.LEFT)

def switchRight():
    # print('switching right')
    pyautogui.click(constant.RIGHT)
