#!/usr/bin/python
# -*- coding: UTF-8 -*-
import platform
import pyautogui

OS = platform.system()
WIDTH, HEIGHT = pyautogui.size()
LEFT = (WIDTH / 4, HEIGHT / 2)
RIGHT = (WIDTH*3 / 4, HEIGHT / 2)

## board
# main Y axis
X1 = 685
X2 = 800
Y1 = 445
Y2 = 550
# positions
INPUT_INCREMENT = (X1, Y1)
BTN_ADD = (X2, Y1)
INPUT_PRICE = (X1, Y2)
BTN_BIT = (X2, Y2)

## prompt
X_1 = 550
X_2 = 750
Y_1 = 550
Y_2 = 630
X_M = 650

INPUT_CODE = (X_2, Y_1)
BTN_REFRESH_CODE = (X_1, Y_1)
BTN_CONFIRM = (X_1, Y_2)
BTN_CANCEL = (X_2, Y_2)
BTN_WRONG_CODE = (X_M, Y_2)
BTN_RESUME = (X_M, Y_2 - 20)
