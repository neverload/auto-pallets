#!/usr/bin/python
# -*- coding: UTF-8 -*-

import pyautogui
import time
import threading
import Image
import pytesseract
import os

import constant
import helper
import setting

class MainTask:

    def _setup(self):
        helper.setPause(setting.NORMAL_PAUSE)
        pyautogui.FAILSAFE = True

    def _clear(self):
        helper.setPause(setting.LONG_PAUSE)
        pyautogui.doubleClick(constant.BTN_RESUME)
        pyautogui.click(constant.BTN_RESUME)
        pyautogui.click(constant.BTN_CANCEL)
        pyautogui.doubleClick(constant.INPUT_INCREMENT,)
        helper.selectAll(None)
        helper.clearInput(None)
        helper.setPause(setting.QUICK_PAUSE)

    def _price(self):
        START_TIME = helper.getTime()
        pyautogui.typewrite(str(setting.INCREMENT), interval=0.01)
        time.sleep(setting.QUICK_PAUSE) # ensure price is entirely entered
        helper.setPause(setting.QUICK_PAUSE)
        loopTime = setting.LOOP_TIME
        while (True):
            now = time.time()
            print('Adding price ... ' + str(setting.INCREMENT) + ' : ' + str(now))
            pyautogui.click(constant.BTN_ADD)
            if now - START_TIME >= loopTime:
                break
        helper.setPause(setting.NORMAL_PAUSE)

        pyautogui.click(constant.BTN_BIT)
        time.sleep(setting.NORMAL_PAUSE) # wait for the prompt
        pyautogui.click(constant.INPUT_CODE)

    def _code(self):
        #helper.switchWindow()
        helper.switchRight()
        while (True):
            code = raw_input('Enter code(empty input means cancel):')
            print(code)
            if (code == 'r'):
                print('Refresh code ...')
                #helper.switchWindow()
                helper.switchLeft()
                pyautogui.doubleClick(constant.BTN_REFRESH_CODE)
                pyautogui.click(constant.INPUT_CODE)
                #helper.switchWindow()
                helper.switchRight()
            elif (len(code) == 0):
                print('Cancel')
                #helper.switchWindow()
                helper.switchLeft()
                pyautogui.doubleClick(constant.BTN_CANCEL)
                #helper.switchWindow()
                helper.switchRight()
                break
            elif (len(code)>0 and len(code)<4):
                print('Insufficient code, retry: ')
            else:
                #helper.switchWindow()
                helper.switchLeft()
                pyautogui.click(constant.INPUT_CODE)
                time.sleep(setting.LONG_PAUSE)
                pyautogui.typewrite(code, interval=0.01)
                pyautogui.click(constant.BTN_CONFIRM)
                #helper.switchWindow()
                helper.switchRight()
                break

    def execute(self):
        self._setup()
        self._clear()
        self._price()
        self._code()


class Capturer (threading.Thread):
    def __init__(self, threadID, threadName):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.threadName = threadName
        self.alive = True
        self.i = 0
    def run(self):
        while self.alive:
            self._captureImage()
    def stop(self):
        self.alive = False
    def _captureImage(self):
        self.i += 1
        priceImg = pyautogui.screenshot(region=(300,1070,102,26)).convert("L")
        timeImg = pyautogui.screenshot(region=(244,1038,140,26)).convert("L")
        price = pytesseract.image_to_string(priceImg)
        time = pytesseract.image_to_string(timeImg)
        print 'Time: ' + time + ', Price: ' + price 

task = MainTask()
task.execute()
