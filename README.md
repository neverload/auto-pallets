# 程序说明

标签（空格分隔）： car

--- 


[TOC]

| 版本    | 描述                    | 日期       | 修订人  | 
| ----    | ---                     | -----      | ---     | 
| 1.0.0   | 基础版本                | 2016-06-10 | 梅止观  | 
| 1.0.1   | 修复 Win 7 bug          | 2016-06-15 | 梅止观  | 


## I：说明
1. 程序原理为屏幕绝对定位，因此一旦定位完成，切勿再改变窗口位置和大小，否则需要重新定位
2. 所有命令均在命令行环境中运行，命令行环境的启动方式
- Windows：开始 -> 搜索框输入 `cmd` -> 回车
- MacOS：启动 '终端'
3. 拍牌/练习时，务必关闭所有无关窗口，否则窗口切换可能被扰乱
4. 拍牌前十五分钟关闭窗口重新打开窗口（防止 flash 内存泄露造成输入遗漏）
5. 程序的研发/测试对象为  [51沪牌模拟程序][7]，因此请打开该模拟程序进行练习
6. 程序优缺点：
    - 全键盘操作：只需操作 方向键 `上`，`回车键`，`数字键`，`R` 键
    - 安装配置较繁琐和专业；需要练习才能熟练

## II：安装
### 安装 `python` 运行时
- Windows：安装 python-2.7.11.msi ([64位版本][3], [32位版本][4])
> 注意：所有组件全部勾选，并且勾选貌似 “Add python.exe to Path” 选项，否则 `python` 命令 和 `pip` 命令无法识别，如下图
> ![components][8]
- MacOS：
    - 安装 Homebrew

    ```
    /usr/bin/ruby -e "$(curl -fsSL     https://raw.githubusercontent.com/Homebrew/install/master/install)"
    ```

    - 安装 python

    ```
    brew install python
    ```
    > 注意：选择 2.7.11 版本（选择 python2，不要选择 python3）

    - 如不成功，尝试安装包 [python-2.7.11-macosx10.6.pkg][9]

### 安装 pyautogui:
Winows & MacOS：在命令行中运行以下命令：
```
pip install pillow
pip install pyautogui
```

### 安装拍牌程序
1. 进入[程序主页][1]，点击 "Downloads" => "Download repository"
2. 解压，把 auto-pallet 目录拷贝到根目录中，例如 C 盘根目录
3. 从终端进入程序目录
    - Windows: `cd C:\auto-pallete`
    - MacOS: `cd ～/auto-pallete`

## III：配置

### 定位配置
1. 固定模拟程序窗口大小和位置，例如占据 2/3 屏幕宽度，另 1/3 用于命令行窗口
2. 打开命令行窗口，输入 `python` + `回车`，进入交互界面（有 '>>>' 提示符的环境）
3. 运行以下命令
    ```
    import pyautogui
    ```

4. 控件定位，用命令 `pyautogui.position()` 进行鼠标定位
    - 定位主界面元素
        - Y1：加价输入框 和 加价按钮 的 Y 轴坐标
        - Y2：出价输入框 和 出价按钮 的 Y 轴坐标
        - X1：加价输入框 和 出价输入框 的 X 轴坐标
        - X2：加价按钮 和 出价按钮 的 X 轴坐标

        ![board-coordinate][2]
    
    - 定位弹出层元素
        - Y_1：验证码输入框 所在行的 Y 轴坐标
        - Y_2：确定 和 取消 按钮所在行的 Y 轴坐标
        - X_M：验证码输入框 的 X 轴坐标
        - X_1：确定 按钮的 X 轴坐标
        - X_2：取消 按钮的 X 周坐标

        ![popup-coordinate][5]

5. 坐标获取如下图

    ![position-capture][6]

6. 将以上坐标写入坐标配置文件 `constant.py`

### 策略配置
按实际需求修改程序文件如下：
1. 加价配置（默认 600，实战建议改为 700）：`setting.py` 中 `INCREMENT` 参数
2. 循环时间配置（默认 1 秒，一般不用修改）：`setting.py` 中 `LOOP_TIME` 参数

## IV：操作
1. 启动命令行窗口(Windows 命令提示符，Mac 终端)
```
Win: 开始 -> 搜索框输入 `cmd` -> 回车
Mac: 启动 '终端'
```
2. 进入程序主目录
```
Win: cd c:\auto-pallet
Mac: cd ~/auto-pallet
```
3. 程序启动命令
```
python main.py
```
4. 操作
    - 运行程序：输入 `python main.py` 或 方向键 `上` 调出上一条命令，回车
    - 验证码：在 **终端** 中输入验证码；不输入验证码直接 `回车` 表示取消；位数不够可重新输入
    - 出价：在目标时刻（价格和时间合适）按 `回车`，自动出价
    - 刷新验证码：如果验证码无法显示，终端输入 `r + 回车`，程序会自动刷新验证码

## V：经验

## 附录 I：Windows 7 bug

Windows 7 运行程序会出现如下错误
```
Traceback (most recent call last):
  File "main.py", line 81, in <module>
    task.execute()
  File "main.py", line 77, in execute
    self._price()
  File "main.py", line 36, in _price
    pyautogui.click(constant.BTN_ADD)
  File "C:\Python27\lib\site-packages\pyautogui\__init__.py", line 362, in click

    platformModule._click(x, y, 'left')
  File "C:\Python27\lib\site-packages\pyautogui\_pyautogui_win.py", line 437, in
 _click
    _sendMouseEvent(MOUSEEVENTF_LEFTCLICK, x, y)
  File "C:\Python27\lib\site-packages\pyautogui\_pyautogui_win.py", line 480, in
 _sendMouseEvent
    raise ctypes.WinError()
WindowsError: [Error 5] 拒绝访问。
```

解决方法：

1. 用记事本打开文件 `<python安装目录>\Lib\site-packages\pyautogui\_pyautogui_win.py`，例如 `C:\Python27\Lib\site-packages\pyautogui\_pyautogui_win.py`
2. 删除或注释第 479-480 行（注释符号为 `#`）
```
    if ctypes.windll.kernel32.GetLastError() != 0:
        raise ctypes.WinError()
```

[1]: https://bitbucket.org/neverload/auto-pallets/
[2]: http://i2.buimg.com/bda4e0a75bc3c8cd.png
[3]: http://pan.baidu.com/s/1hrUthqs
[4]: http://pan.baidu.com/s/1c1KXRn6
[5]: http://i4.buimg.com/715308211b56d026.png
[6]: http://i1.buimg.com/16471f639a5ba512.png
[7]: http://moni.51hupai.org
[8]: http://i4.buimg.com/40e5dd1c804720b4.png
[9]: http://pan.baidu.com/s/1dFx5DtB